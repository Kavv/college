<!DOCTYPE html>
<html>
<head>
    <title>{{$data['title']}}</title>
</head>
<body>
    <h1 style='text-align: center; font-size: 20px'>{{$data['title']}}</h1>
    <p style='text-align: center'>{{$data['subtitle']}}</p>
    <table style="width: 100%; font-size: 15px;">
        <thead style='border: 1px solid; border-right: none!important;'>
            <tr>
                @foreach($data['keys'] as $index => $key) 
                    <th style='border-right: 1px solid; width: <?php echo $data['cellWidth'][$index]; ?>;'>{{ $key }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody style='border: 1px solid; border-right: none!important;'>
            @foreach($data['students'] as $student)
                <tr>
                    @foreach($data['keys'] as $index => $key) 
                        <td style='border-right: 1px solid; border-bottom: 1px solid; width: <?php echo $data['cellWidth'][$index]; ?>;'>{{ $student->$key }}</td>
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>