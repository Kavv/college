<!DOCTYPE html>
<html>
<head>
    <title>{{$data['title']}}</title>
</head>
<body>
    <h1 style='text-align: center; font-size: 20px'>{{$data['title']}}</h1>
    @foreach($data['assistances'] as $groupKey => $group)
        <div>
            <h2 style='text-align: center; font-weight: bold;'>GRUPO: {{$group['nombre']}}</h2>
            <table style="width: 100%; font-size: 15px;">
                <thead style='border: 1px solid; border-right: none!important;'>
                    <tr>
                        <th style='border-right: 1px solid; width: 20%;'>Fecha</th>
                        <th style='border-right: 1px solid; width: 20%;'>Estado</th>
                        <th style='border-right: 1px solid; width: 30%;'>Profesor</th>
                        <th style='border-right: 1px solid; width: 20%;'>Digitado</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($group['estudiantes'] as $studenKey => $studentData)
                        <tr>
                            <td colspan="4" style='border: 1px solid; font-weight: bold;'>ALUMNO: {{$studentData[0]->Estudiante}} / USUARIO: {{$studentData[0]->Usuario}}</td>
                        </tr>
                        <?php $presente = $justificado = $ausente = 0; ?>
                        @foreach($studentData as $assistance)
                            <tr style='border: 1px solid; border-right: none!important;'>
                                <td style='border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid; width:20%'>{{$assistance->Fecha}}</td>
                                <td style='border-right: 1px solid; border-bottom: 1px solid; width:20%'>{{$assistance->Estado}}</td>
                                <td style='border-right: 1px solid; border-bottom: 1px solid; width:30%'>{{$assistance->Profesor}}</td>
                                <td style='border-right: 1px solid; border-bottom: 1px solid; width:20%'>{{$assistance->Digitado}}</td>
                            </tr>
                            <?php  
                                switch ($assistance->Estado) {
                                    case 'PRESENTE':
                                        $presente++;
                                        break;
                                    
                                    case 'AUSENTE':
                                        $ausente++;
                                        break;
        
                                    case 'JUSTIFICADO':
                                        $justificado++;
                                        break;
                                }
                            ?>
                        @endforeach
                        <tr>
                            <td style='border: 1px solid; font-weight: bold;'>Presente: {{$presente}}</td>
                            <td style='border: 1px solid; font-weight: bold;'>Justificado: {{$justificado}}</td>
                            <td style='border: 1px solid; font-weight: bold;'>Ausente: {{$ausente}}</td>
                        </tr>
                        <div style="height: 20px"></div>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endforeach
</body>
</html>