<?php

use App\Http\Controllers\Accounting\AccountsController;
use App\Http\Controllers\Accounting\AccountTypeController;
use App\Http\Controllers\Accounting\MovementsController;
use App\Http\Controllers\Accounting\MovementTypeController;
use App\Http\Controllers\AreasController;
use App\Http\Controllers\AssignmentsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CycleController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\StudentsController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\AssistanceController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\ReportController;
use App\Models\Assistance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'users'],function() {
    //RESOURCES
    Route::get('/', [UserController::class, 'index']);
    Route::get('/list/{type}', [UserController::class, 'studentList']);
    Route::get('/list/by-type/{type}', [UserController::class, 'usersByType']);
    Route::get('/current/areas', [UserController::class, 'getTeacherAreas']);
    Route::get('/current/group', [UserController::class, 'getTeacherGroup']);
    Route::post('/', [UserController::class, 'store']);
    Route::get('/create', [UserController::class, 'create']);
    Route::get('/{valor}', [UserController::class, 'show']);
    Route::get('/{valor}/edit', [UserController::class, 'edit']);
    Route::put('/{valor}', [UserController::class, 'update']);
    Route::delete('/{valor}', [UserController::class, 'destroy']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/register', [AuthController::class, 'register'])->name('signup');
    Route::post('/login', [AuthController::class, 'login'])->name('signin');
    Route::post('/logout', [AuthController::class, 'logout'])->name('signout');
    Route::post('/refresh', [AuthController::class, 'refresh'])->name('refresh');
    Route::get('/me', [AuthController::class, 'me'])->name('me');
    Route::post('/changePassword', [AuthController::class, 'changePassword'])->name('changePassword');
});

Route::group([
//    'middleware' => 'jwt.verify',
    'prefix' => 'cycles'
], function ($router) {
   Route::get('/list', [CycleController::class, 'index'])->name('list-cycles');
   Route::post('/add', [CycleController::class, 'create'])->name('add-cycle');
   Route::post('/structure', [CycleController::class, 'generateStructure'])->name('generate-structure');
   Route::post('/structure/get', [CycleController::class, 'getStructure'])->name('get-structure');
   Route::put('/edit/{id}', [CycleController::class, 'update'])->name('edit-cycle');
   Route::delete('/delete/{id}', [CycleController::class, 'delete'])->name('delete-cycle');
   });

Route::group(['prefix' => 'payments'],function() {
    //RESOURCES
    Route::get('/', [PaymentsController::class, 'index']);
    Route::post('/add', [PaymentsController::class, 'addStudentPayment']);
    Route::post('/', [PaymentsController::class, 'store']);
    Route::put('/{valor}', [PaymentsController::class, 'update']);
    Route::delete('/{valor}', [PaymentsController::class, 'destroy']);
    Route::get('/userPayments', [PaymentsController::class, 'userPayments'])->name('userPayments');
    Route::put('/userPayments/{valor}', [PaymentsController::class, 'editUserPayments'])->name('editUserPayments');
    Route::delete('/userPayments/{valor}', [PaymentsController::class, 'deleteUserPayments'])->name('deleteUserPayments');
});

Route::group([
//    'middleware' => 'jwt.verify',
    'prefix' => 'teachers'
], function ($router) {
    Route::get('/list', [TeacherController::class, 'index'])->name('list-teachers');
    Route::post('/add', [TeacherController::class, 'create'])->name('add-teacher');
    Route::put('/edit/{id}', [TeacherController::class, 'update'])->name('edit-teacher');
    Route::delete('/delete/{id}', [TeacherController::class, 'delete'])->name('delete-teacher');
    Route::post('/assign-group', [TeacherController::class, 'assignGroup'])->name('assign-group-to-teacher');
    Route::post('/unassign-group', [TeacherController::class, 'unassignGroup'])->name('unassign-group-to-teacher');
    Route::get('/assignments-received/{id}', [TeacherController::class, 'assignmentsReceived']);
    Route::put('/assignments-received/{id}', [TeacherController::class, 'setAssignmentsPoints']);
});

Route::group([
    'prefix' => 'groups',
//    'middleware' => 'jwt.verify'
], function ($router) {
   Route::get('/list', [GroupController::class, 'index'])->name('list-groups');
   Route::get('/by-cycle/{id}', [GroupController::class, 'getByCycle'])->name('getByCycle');
   Route::post('/add', [GroupController::class, 'create'])->name('add-group');
   Route::put('/edit/{id}', [GroupController::class, 'update'])->name('edit-group');
   Route::delete('/delete/{id}', [GroupController::class, 'delete'])->name('delete-group');
});

Route::group([
    'prefix' => 'areas',
    // 'middleware' => 'jwt.verify'
], function ($router) {
   Route::get('/', [AreasController::class, 'index']);
   Route::get('/list', [AreaController::class, 'index'])->name('list-areas');
   Route::post('/add', [AreaController::class, 'create'])->name('add-area');
   Route::put('/edit/{id}', [AreaController::class, 'update'])->name('edit-area');
   Route::delete('/delete/{id}', [AreaController::class, 'delete'])->name('delete-area');
});


Route::group([
    'prefix' => 'contents',
    // 'middleware' => 'jwt.verify'
], function ($router) {
    Route::get('/list', [ContentController::class, 'index'])->name('list-contents');
    Route::post('/add', [ContentController::class, 'create'])->name('add-content');
    Route::put('/edit/{id}', [ContentController::class, 'update'])->name('edit-content');
    Route::delete('/delete/{id}', [ContentController::class, 'delete'])->name('delete-content');
});

Route::group([
    'prefix' => 'documents',
    // 'middleware' => 'jwt.verify'
], function ($router) {
    Route::post('/upload/{cycle_id}/{group_id}/{area_id}/{content_id}', [DocumentController::class, 'upload'])->name('upload-document');
    Route::post('/uploadForAssignment/{cycle_id}/{group_id}/{area_id}/{assignment_id}', [DocumentController::class, 'uploadForAssignment'])->name('uploadForAssignment');
    Route::delete('/delete/{id}', [DocumentController::class, 'delete'])->name('delete-document');
});
Route::group(['prefix' => 'student'],function() {
    Route::get('/my-payments', [StudentsController::class, 'payments']);
    Route::get('/my-assignments', [StudentsController::class, 'assignments']);
    Route::post('/send-assignment', [StudentsController::class, 'sendAssignment']);
    Route::get('/get-assignment/{id}', [StudentsController::class, 'getAssignment']);
    Route::get('/get-contents/{id}', [StudentsController::class, 'getContents']);
});

Route::group(['prefix' => 'assignments'],function() {
    //RESOURCES
    Route::get('/', [AssignmentsController::class, 'index']);
    Route::post('/', [AssignmentsController::class, 'store']);
    Route::put('/{valor}', [AssignmentsController::class, 'update']);
    Route::delete('/{valor}', [AssignmentsController::class, 'destroy']);
});

Route::group(['prefix' => 'accounting'], function () {
    // Accounts
   Route::get('/accounts', [AccountsController::class, 'index'])->name('list-accounts');
   Route::post('/accounts', [AccountsController::class, 'create'])->name('create-accounts');
   Route::put('/accounts/{id}', [AccountsController::class, 'update'])->name('update-accounts');
   Route::delete('/accounts/{id}', [AccountsController::class, 'delete'])->name('delete-accounts');

   // Account Types
    Route::get('/account-types', [AccountTypeController::class, 'index'])->name('list-account-types');
    Route::post('/account-types', [AccountTypeController::class, 'create'])->name('create-account-types');
    Route::put('/account-types/{id}', [AccountTypeController::class, 'update'])->name('update-account-types');
    Route::delete('/account-types/{id}',[AccountTypeController::class, 'delete'])->name('delete-account-types');

    // Movement Types
    Route::get('/movement-types', [MovementTypeController::class, 'index'])->name('list-movement-types');
    Route::post('/movement-types', [MovementTypeController::class, 'create'])->name('create-movement-types');
    Route::put('/movement-types/{id}', [MovementTypeController::class, 'update'])->name('update-movement-types');
    Route::delete('/movement-types/{id}', [MovementTypeController::class, 'delete'])->name('delete-movement-types');

    // Movements
    Route::get('/movements', [MovementsController::class, 'index'])->name('list-movements');
    Route::post('/movements', [MovementsController::class, 'create'])->name('create-movements');
    Route::put('/movements/{id}', [MovementsController::class, 'update'])->name('update-movements');
    Route::delete('/movements/{id}', [MovementsController::class, 'delete'])->name('delete-movements');
});

Route::group(['prefix'=>'assistances'], function () {
    Route::get('/', [AssistanceController::class, 'index'])->name('list-assistances');
    Route::post('/', [AssistanceController::class, 'create'])->name('create-assistance');
    Route::put('/{id}', [AssistanceController::class, 'update'])->name('update-assistances');
    Route::delete('/{id}', [AssistanceController::class, 'delete'])->name('delete-assistance');
});


Route::group(['prefix'=>'reports'], function () {
    Route::post('/getStudentsByDateRange', [ReportController::class, 'getStudentsByDateRange']);
    Route::post('/getStudentsByCycle', [ReportController::class, 'getStudentsByCycle']);
    Route::post('/getStudentsByGroup', [ReportController::class, 'getStudentsByGroup']);
    Route::post('/getAssistanceByStudent', [ReportController::class, 'getAssistanceByStudent']);
    Route::post('/getAssistanceByGroup', [ReportController::class, 'getAssistanceByGroup']);
    Route::post('/getAssistanceByCycle', [ReportController::class, 'getAssistanceByCycle']);
});

