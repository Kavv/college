<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_scores', function (Blueprint $table) {
            $table->integer('document_id')->unsigned();
            $table->integer('score_id')->unsigned();
            $table->foreign('score_id')
            ->references('id')->on('scores')
            ->onDelete('cascade');
            $table->foreign('document_id')
            ->references('id')->on('documents')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_scores');
    }
}
