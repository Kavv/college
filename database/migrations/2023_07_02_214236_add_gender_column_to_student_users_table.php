<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGenderColumnToStudentUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_user', function (Blueprint $table) {
            $table->string('gender', 20)->nullable();
            $table->string('phone', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_user', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('phone');
        });
    }
}
