<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_documents', function (Blueprint $table) {
            $table->integer('content_id')->unsigned();
            $table->integer('document_id')->unsigned();
            $table->foreign('content_id')
            ->references('id')->on('contents')
            ->onDelete('cascade');
            $table->foreign('document_id')
            ->references('id')->on('documents')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_documents');
    }
}
