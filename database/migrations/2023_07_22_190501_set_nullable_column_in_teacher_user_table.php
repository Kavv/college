<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetNullableColumnInTeacherUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teacher_user', function (Blueprint $table) {
            $table->string('gender', 20)->nullable()->change();
            $table->date('birthday', )->nullable()->change();
            $table->string('dni', 20)->nullable()->change();
            $table->string('phone', 30)->nullable()->change();
            $table->string('institution', 30)->nullable()->change();
            $table->float('salary', 2)->nullable()->change();
            $table->string('address', 100)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teacher_user', function (Blueprint $table) {
            //
        });
    }
}
