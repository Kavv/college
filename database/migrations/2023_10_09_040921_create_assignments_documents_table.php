<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments_documents', function (Blueprint $table) {
            $table->integer('assignment_id')->unsigned();
            $table->integer('document_id')->unsigned();
            $table->foreign('assignment_id')
            ->references('id')->on('assignments')
            ->onDelete('cascade');
            $table->foreign('document_id')
            ->references('id')->on('documents')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments_documents');
    }
}
