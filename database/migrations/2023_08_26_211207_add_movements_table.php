<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('movements', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('description', 250);
            $table->string('currency', 20);
            $table->float('amount',2);
            $table->string('notes', 250)->nullable();
            $table->integer('type')->unsigned()->nullable();
            $table->foreign('type')
                ->references('id')->on('movement_type')
                ->onDelete('cascade');
            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->integer('origin_account')->unsigned()->nullable();
            $table->foreign('origin_account')
                ->references('id')->on('accounts')
                ->onDelete('cascade');
            $table->integer('destination_account')->unsigned()->nullable();
            $table->foreign('destination_account')
                ->references('id')->on('accounts')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('movements');
    }
}
