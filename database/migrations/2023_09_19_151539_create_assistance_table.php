<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssistanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistance', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('status', 20);
            $table->integer('teacher_id')->unsigned()->nullable();
            $table->foreign('teacher_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
            $table->integer('student_id')->unsigned()->nullable();
            $table->foreign('student_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
            $table->integer('group_id')->unsigned()->nullable();
            $table->foreign('group_id')
            ->references('id')->on('groups')
            ->onDelete('cascade');
            $table->integer('cycle_id')->unsigned()->nullable();
            $table->foreign('cycle_id')
            ->references('id')->on('cycles')
            ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistance');
    }
}
