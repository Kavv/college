<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    // Group Controller

    public function index() {
        return Group::orderBy('id', 'asc')->get();
    }

    public function create(Request $request) {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        return Group::create($request->all());
    }

    public function update(Request $request, $id) {
        $group = Group::find($id);
        $group->update($request->all());

        return $group;
    }

    public function delete(Request $request, $id) {
        return Group::destroy($id);
    }

    public function getByCycle($cycle_id) {
        return Group::where('cycle_id', '=', $cycle_id)->orderBy('name')->get();
    }
}
