<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Documents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Payment;
use App\Models\Score;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class StudentsController extends Controller
{
    public function assignments() {
        $user = Auth::user();
        $student = DB::table('group_users as gu')
            ->select('gu.group_id')
            ->where('gu.user_id', $user->id)
            ->whereNull('gu.deleted_at')
            ->first();
        $myAssignments = Assignment::join('assignments_groups as ag', 'ag.assignment_id', '=', 'assignments.id')
        ->join('groups', 'ag.group_id', '=', 'groups.id')
        ->join('cycles', 'cycles.id', '=', 'groups.cycle_id')
        ->leftJoin('scores', function($join) use ($user) {
            $join->on('scores.assignment_id', '=', 'assignments.id');
            $join->on('scores.user_id', '=', DB::RAW($user->id));
        })
        ->select([
            'assignments.id',
            'assignments.name',
            'assignments.description',
            'assignments.limit_time as limitTime',
            'assignments.start_date as startDate',
            'assignments.url',
            'assignments.points',
            'assignments.area_id as area',
            'groups.id as group',
            'groups.name as groupName',
            'cycles.id as cycle',
            'cycles.name as cycleName',
            'assignments.created_at as createdAt',
            'scores.points as result',
            'scores.id as scoreId',
        ])
        ->where('groups.id', $student->group_id)
        ->get();
        return $myAssignments;
    }

    public function payments()
    {
        $paymentsDone = DB::table('payment_users as pu')
        ->join('users as u', 'u.id', '=', 'pu.user_id')
        ->join('payments as p', 'p.id', '=', 'pu.payment_id')
        ->select([
            DB::RAW("'PAGADO' as status"),
            'pu.id', 
            'pu.date',
            'pu.created_at as createdAt',
            'pu.payment as amount',
            'pu.quantity',
            'pu.discount',
            'pu.total',
            'u.id as userId',
            'u.user',
            'u.name as userName',
            'p.id as paymentId',
            'p.name as paymentName',
        ])
        ->where([
            'pu.deleted_at' => null,
            'pu.user_id' => Auth::user()->id,
            //'cycle_id' => 1
        ])
        ->orderBy('date', 'desc')
        ->get();
        
        $ids = $paymentsDone->pluck('paymentId');
        $pendingPayments = DB::table('payments as p')
            ->whereNotIn('id',$ids)
            ->where(['mandatory' => true])
            ->select([
                DB::RAW("'PENDIENTE' as status"),
                DB::RAW("'-' as date"),
                DB::RAW("'-' as quantity"),
                DB::RAW("'-' as discount"),
                DB::RAW("'-' as total"),
                'p.id as paymentId',
                'p.name as paymentName',
                'p.amount as amount',
                'p.mandatory',
                'p.priority',
            ])
            ->orderBy('priority', 'asc')
            ->get();

        $sortedData = $paymentsDone->concat($pendingPayments);
        $resultArray = $sortedData->values()->all();
        return $resultArray;
    }


    public function sendAssignment(Request $request) {
        $assignment = Assignment::find($request->get('assignment'));

        $limitTime = Carbon::parse($assignment->limit_time);
        $now = Carbon::now();

        if ($limitTime->lt($now)) {
            return response()->json([
                'message' => 'El tiempo de entrega ya ha culminado'
            ], 422);
        }

        if (!$request->get('record')) {
            $score = Score::create([
                'assignment_id' => $request->get('assignment'),
                'user_id' => Auth::user()->id,
                'answer' => $request->get('answer'),
                //'file' => $request->get('file'),
                //'points' => $request->get(''),
                //'comments' => $request->get(''),
                //'explanation' => $request->get(''),
            ]);
        } else {
            $score = Score::find($request->get('record'));
            $score->update([
                'answer' => $request->get('answer'),
                'file' => $request->get('file'),
            ]);
            $score->save();
        }
        return response()->json($score, 200);
    }

    public function getAssignment($id) {
        return Score::select([
            'scores.id',
            'scores.assignment_id as assignment',
            'scores.user_id as student',
            'scores.points',
            'scores.comments',
            'scores.explanation',
            'scores.answer',
            'scores.file',
            'scores.created_at as createdAt',
        ])->find($id);
    }

    public function getContents($id) {
        $user = Auth::user();
        $student = DB::table('group_users as gu')
            ->select('gu.group_id')
            ->where('gu.user_id', $user->id)
            ->whereNull('gu.deleted_at')
            ->first();

        $contents = DB::table('contents as c')
            ->leftJoin('content_groups as cg', 'c.id', '=', 'cg.content_id')
            ->where([
                'c.area_id' => $id,
                'cg.group_id' => $student->group_id,
            ])->get();

        
        foreach ($contents as $content) {
            $content->documents = Documents::join('content_documents as cd', 'cd.document_id', '=', 'documents.id')->where('cd.content_id', $content->id)->get();
        }

        return $contents;
    }
}
