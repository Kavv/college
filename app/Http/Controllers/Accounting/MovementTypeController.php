<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use App\Models\MovementType;
use Illuminate\Http\Request;

class MovementTypeController extends Controller
{
    //

    public function index() {
        return MovementType::orderBy('id', 'asc')->get();
    }

    public function create(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        return MovementType::create($request->all());
    }

    public function update(Request $request, $id) {
        $movement_type = MovementType::find($id);
        $movement_type->update($request->all());

        return $movement_type;
    }

    public function delete(Request $request, $id) {
        return MovementType::destroy($id);
    }
}
