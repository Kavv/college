<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use App\Models\Accounts;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    //
    public function index() {
        return Accounts::orderBy('id', 'asc')->get();
    }

    public function create(Request $request) {
        $request->validate([
            'code' => 'required',
            'name' => 'required',
            'type_id' => 'required'
        ]);

        return Accounts::create($request->all());
    }

    public function update(Request $request, $id) {
        $account = Accounts::find($id);

        $account->update($request->all());

        return $account;
    }

    public function delete(Request $request, $id) {
        return Accounts::destroy($id);
    }
}
