<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use App\Models\Movements;
use Illuminate\Http\Request;

class MovementsController extends Controller
{
    //
    public function index() {
        return Movements::orderBy('id', 'asc')->get();
    }

    public function create(Request $request) {
        $request->validate([
            'date' => 'required',
            'description' => 'required',
            'currency' => 'required',
            'amount' => 'required',
            'created_by' => 'required',
            'origin_account' => 'required',
            'destination_account' => 'required'
        ]);

        return Movements::create($request->all());
    }

    public function update(Request $request, $id) {
        $movement = Movements::find($id);
        $movement->update($request->all());

        return $movement;
    }

    public function delete(Request $request, $id) {
        return Movements::destroy($id);
    }
}
