<?php

namespace App\Http\Controllers\Accounting;

use App\Http\Controllers\Controller;
use App\Models\AccountType;
use Illuminate\Http\Request;

class AccountTypeController extends Controller
{
    //

    public function index() {
        return AccountType::orderBy('id', 'asc')->get();
    }

    public function create(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        return AccountType::create($request->all());
    }

    public function update(Request $request, $id) {
        $account_type = AccountType::find($id);
        $account_type->update($request->all());

        return $account_type;
    }

    public function delete(Request $request, $id) {
        return AccountType::destroy($id);
    }
}
