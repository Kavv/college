<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\StudentUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

class UserController extends Controller
{
    public function index() {
        return User::orderBy('id', 'asc')->get();
    }
    public function studentList($type) {
        $users = User::where('type_id', $type);
        $users = $type > 2 ? $users->orWhere('type_id', '>', 2)->get() : $users->get();
        return $users;
    }

    public function usersByType($type) {
        return User::where('type_id', $type)->get();
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

//            Log::info($request->all());
            $user = User::create([
                'name' => $request->get('name'),
                'user' => $request->get('code'),
                'password' => bcrypt(request()->password),
                'type_id' => $request->get('type'),
            ]);
            if ($request->get('type') === 1) {
                $user->studentData()->create([
                    'father' => $request->get('father') ?? null,
                    'mother' => $request->get('mother') ?? null,
                    'father_phone' => $request->get('fatherPhone') ?? null,
                    'mother_phone' => $request->get('motherPhone') ?? null,
                    'birthday' => $request->get('birthday') ?? null,
                    'gender' => $request->get('gender') ?? null,
                    'phone' => $request->get('phone') ?? null,
                    'note' => $request->get('note') ?? null,
                ]);
                $user->studentGroups()->attach(
                    $request->get('group')
                );
            }
            if ($request->get('type') === 2) {
                $user->teacherData()->create([
                    'gender' => $request->get('gender') ?? null,
                    'birthday' => $request->get('birthday') ?? null,
                    'dni' => $request->get('dni') ?? null,
                    'phone' => $request->get('phone') ?? null,
                    'title' => $request->get('title') ?? null,
                    'institution' => $request->get('institution') ?? null,
                    'salary' => $request->get('salary') ?? null,
                    'address' => $request->get('address') ?? null,
                ]);
            }
            if ($request->get('type') > 2) {
                $user->staffData()->create([
                    'gender' => $request->get('gender') ?? null,
                    'birthday' => $request->get('birthday') ?? null,
                    'dni' => $request->get('dni') ?? null,
                    'phone' => $request->get('phone') ?? null,
                    'position' => $request->get('position') ?? null,
                    'salary' => $request->get('salary') ?? null,
                    'address' => $request->get('address') ?? null,
                ]);
            }
            DB::commit();
            return response()->json($user, 200);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => $th
            ], 500);
        }
    }

    public function create() {


    }

    public function show($id) {
        $result = new stdClass();
        $user = User::find($id);
        $result->id = $user->id;
        $result->user = $user->user;
        $result->name = $user->name;
        $result->type = $user->type_id;
        switch ($user->type_id) {
            case 1:
                $studentData = $user->studentData;
                if ($studentData) {
                    $result->father = $studentData->father;
                    $result->mother = $studentData->mother;
                    $result->father_phone = $studentData->father_phone;
                    $result->mother_phone = $studentData->mother_phone;
                    $result->birthday = $studentData->birthday;
                    $result->note = $studentData->note;
                    $result->gender = $studentData->gender;
                    $result->phone = $studentData->phone;
                    $groups = DB::table('group_users')
                    ->leftJoin('groups', 'groups.id', '=', 'group_id')
                    ->where(['user_id' => $result->id, 'group_users.deleted_at' => null])
                    ->select('groups.id as group', 'groups.cycle_id as cycle')
                    ->first();
                    if($groups) {
                        $result->group = $groups->group;
                        $result->cycle = $groups->cycle;
                    }
                }
                break;
            case 2:
                $teacherData = $user->teacherData;
                $result->gender = $teacherData->gender ?? null;
                $result->birthday = $teacherData->birthday ?? null;
                $result->dni = $teacherData->dni ?? null;
                $result->phone = $teacherData->phone ?? null;
                $result->title = $teacherData->title ?? null;
                $result->institution = $teacherData->institution ?? null;
                $result->salary = $teacherData->salary ?? null;
                $result->address = $teacherData->address ?? null;
                break;
            case 3:
                $teacherData = $user->staffData;
                $result->gender = $teacherData->gender ?? null;
                $result->birthday = $teacherData->birthday ?? null;
                $result->dni = $teacherData->dni ?? null;
                $result->phone = $teacherData->phone ?? null;
                $result->position = $teacherData->position ?? null;
                $result->salary = $teacherData->salary ?? null;
                $result->address = $teacherData->address ?? null;
                break;
            case 4:
                $teacherData = $user->staffData;
                $result->gender = $teacherData->gender ?? null;
                $result->birthday = $teacherData->birthday ?? null;
                $result->dni = $teacherData->dni ?? null;
                $result->phone = $teacherData->phone ?? null;
                $result->position = $teacherData->position ?? null;
                $result->salary = $teacherData->salary ?? null;
                $result->address = $teacherData->address ?? null;
                break;
        }
        return $result;
    }

    public function edit($id) {

    }

    public function update($id, Request $request) {
        $user = User::find($id);
        $user->name = $request->get("name");
        $user->user = $request->get("user");
        if ($request->get("password") != "") {
            $user->password = Hash::make($request->get('password'));
        }
        switch ($user->type_id) {
            case 1:
                $user->studentData()->update([
                    'father' => $request->get('father') ?? null,
                    'mother' => $request->get('mother') ?? null,
                    'father_phone' => $request->get('fatherPhone') ?? null,
                    'mother_phone' => $request->get('motherPhone') ?? null,
                    'birthday' => $request->get('birthday') ?? null,
                    'gender' => $request->get('gender') ?? null,
                    'phone' => $request->get('phone') ?? null,
                    'note' => $request->get('note') ?? null,
                ]);
                DB::table('group_users')
                ->where('user_id', $id)
                ->update(['deleted_at' => Carbon::now()]);

                DB::table('group_users')
                ->updateOrInsert(
                    [
                        'user_id' => $id, 
                        'group_id' => $request->get('group')
                    ],
                    [
                        'user_id' => $id,
                        'group_id' => $request->get('group'),
                        'deleted_at' => null,
                    ]
                );
                break;
            case 2:
                $user->teacherData()->update([
                    'gender' => $request->get('gender') ?? null,
                    'birthday' => $request->get('birthday') ?? null,
                    'dni' => $request->get('dni') ?? null,
                    'phone' => $request->get('phone') ?? null,
                    'title' => $request->get('title') ?? null,
                    'institution' => $request->get('institution') ?? null,
                    'salary' => $request->get('salary') ?? null,
                    'address' => $request->get('address') ?? null,
                ]);
                break;
            case 3:
                $user->staffData()->update([
                    'gender' => $request->get('gender') ?? null,
                    'birthday' => $request->get('birthday') ?? null,
                    'dni' => $request->get('dni') ?? null,
                    'phone' => $request->get('phone') ?? null,
                    'position' => $request->get('position') ?? null,
                    'salary' => $request->get('salary') ?? null,
                    'address' => $request->get('address') ?? null,
                ]);
                $user->type = $request->get('type');
                break;
            case 4:
                $user->staffData()->update([
                    'gender' => $request->get('gender') ?? null,
                    'birthday' => $request->get('birthday') ?? null,
                    'dni' => $request->get('dni') ?? null,
                    'phone' => $request->get('phone') ?? null,
                    'position' => $request->get('position') ?? null,
                    'salary' => $request->get('salary') ?? null,
                    'address' => $request->get('address') ?? null,
                ]);
                $user->type_id = $request->get('type');
                break;

            default:
                # code...
                break;
        }

        $user->save();
    }

    public function destroy($id) {
        $user = User::find($id);
        $user->delete();
        return response()->noContent(200);
    }


    public function getTeacherAreas() {
        $user = User::find(Auth::user()->id);
        return $user->teacherAreas;
    }

    public function getTeacherGroup() {
        $id = Auth::user()->id;
        $group = DB::table('users as u')
        ->join('teacher_user as t', 't.user_id', '=', 'u.id')
        ->join('groups as g', 'g.id', '=', 't.group_id')
        ->select('g.id', 'g.name')
        ->where('u.id', $id)
        ->first();
        return $group;
    }
}
