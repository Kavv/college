<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Documents;
use App\Models\ContentDocument;
use Illuminate\Support\Facades\DB;
use File;

class DocumentController extends Controller
{
    public function upload(Request $request, $cycle_id, $group_id, $area_id, $content_id) {
        if ($cycle_id && $group_id && $area_id && $content_id && $request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $destinationPath = public_path() . '/contents/' . $cycle_id . '/' . $group_id . '/' . $area_id . '/' . $content_id;
            $file->move($destinationPath, $fileName);
            $document = new Documents;
            $document->name = $fileName;
            $document->url = $destinationPath . '/' . $fileName;
            $temp = explode('.', $fileName);
            $fileType = end(($temp));
            $document->type = $fileType;
            $document->save();

            $content_document = DB::table('content_documents')->insert([
                'content_id' => $content_id,
                'document_id' => $document->id
            ]);
            // $content_document->content_id = $content_id;
            // $content_document->document_id = $document->id;
            // $content_document->save();

            return $document;
        }
    }

    public function uploadForAssignment(Request $request, $cycle_id, $group_id, $area_id, $score_id) {
        if ($cycle_id && $group_id && $area_id && $score_id && $request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $destinationPath = public_path() . '/contents/' . $cycle_id . '/' . $group_id . '/' . $area_id . '/' . $score_id;
            $file->move($destinationPath, $fileName);
            $document = new Documents;
            $document->name = $fileName;
            $document->url = $destinationPath . '/' . $fileName;
            $temp = explode('.', $fileName);
            $fileType = end(($temp));
            $document->type = $fileType;
            $document->save();

            $content_document = DB::table('documents_scores')->insert([
                'score_id' => $score_id,
                'document_id' => $document->id
            ]);

            return $document;
        }
    }

    public function delete(Request $request, $id) {
        return Documents::destroy($id);
    }
}
