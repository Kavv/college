<?php

namespace App\Http\Controllers;

use App\Models\Documents;
use App\Models\Group;
use App\Models\GroupUser;
use App\Models\Score;
use App\Models\TeacherUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TeacherController extends Controller
{
    //
    public function index() {
        $teachers = TeacherUser::orderBy('id', 'asc')->get();
        return response()->json($teachers, 200);
    }

    public function create(Request $request) {
        $request->validate([
            'user_id' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'dni' => 'required',
            'phone' => 'required',
            'title' => 'required',
            'institution' => 'required',
            'salary' => 'required',
            'address' => 'required',
        ]);

        return TeacherUser::create($request->all());
    }

    public function update(Request $request, $id) {
        $teacher = TeacherUser::find($id);
        $teacher->update($request->all());

        return $teacher;
    }

    public function delete(Request $request, $id) {
        return TeacherUser::destroy($id);
    }

    public function assignGroup(Request $request) {
        $request->validate([
            'teacher_id' => 'required',
            'group_id' => 'required'
        ]);

        $teacher =  TeacherUser::where('user_id', $request->teacher_id)->first();

        $group_user = new GroupUser();
        $group_user->group_id = $request->group_id;
        $group_user->user_id = $request->teacher_id;
        $group_user->save();
        $teacher->group_id = $request->group_id;
        $teacher->save();

        $group = Group::where('id', $request->group_id)->first();

        return $group;
    }

    public function unassignGroup(Request $request) {
        $request->validate([
            'teacher_id' => 'required',
            'group_id' => 'required'
        ]);

        $group_user = GroupUser::where('user_id', '=', $request->teacher_id)
            ->where('group_id', '=', $request->group_id)
            ->first();

        
        return GroupUser::destroy($group_user->id);
    }

    public function assignmentsReceived($id) {
        $assignments = DB::table('assignments as a')
            ->join('assignments_groups as g', 'g.assignment_id', '=', 'a.id')
            ->leftJoin('group_users as gu', function($join) {
                $join->on('gu.group_id', '=', 'g.group_id');
                $join->whereNull('gu.deleted_at');
            })
            ->leftJoin('users as u', 'u.id', '=', 'gu.user_id')
            ->leftJoin('scores as s', function($join) {
                $join->on('s.assignment_id', '=', 'a.id');
                $join->on('s.user_id', '=', 'u.id');
            })
            ->where('a.id', '=', $id)
            ->select([
                's.id',
                's.answer',
                's.file',
                'u.id as student',
                'u.name as studentName',
                'a.name as assigmentName',
                'a.points as maxPoints',
                's.points as points',
                's.comments as comments',
            ])
            ->get();

        foreach ($assignments as $key => $result) {
            $result->documents = Documents::join('documents_scores as ds', 'ds.document_id', '=', 'documents.id')->where('ds.score_id', $result->id)->get();
        }

        return response()->json($assignments, 200);
    }

    public function setAssignmentsPoints(Request $request, $id) {
        $score = Score::find($id);
        $score->comments = $request->get('comments');
        $score->points = $request->get('points');
        
        $score->save();
        $data = $this->getAssignmentScore($id);
        return response()->json($data, 200);
            
    }

    public function getAssignmentScore($id) {
        return DB::table('scores as s')
            ->join('users as u', 'u.id', '=', 's.user_id')
            ->join('assignments as a', 'a.id', '=', 's.assignment_id')
            ->where('s.id', '=', $id)
            ->select([
                's.id',
                's.answer',
                's.file',
                'u.id as student',
                'u.name as studentName',
                'a.name as assigmentName',
                'a.points as maxPoints',
                's.points as points',
                's.comments as comments',
            ])
            ->first();
    }
}
