<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Payment;
use App\Models\User;


class PaymentsController extends Controller
{
    public function index()
    {
        $payments = Payment::all();
        
        $nonNullValues = $payments->reject(fn($item) => $item->priority === null);
        $nullValues = $payments->filter(fn($item) => $item->priority === null);
        $sortedData = $nonNullValues->sortBy('priority');
        $sortedNullData = $nullValues->sortBy('id');
        $sortedData = $sortedData->concat($sortedNullData);
        $resultArray = $sortedData->values()->all();
        return $resultArray;
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $user = Payment::create([
            'name' => $request->get('name'),
            'amount' => $request->get('amount'),
            'mandatory' => $request->get('mandatory'),
        ]);
        DB::commit();
        return response()->json($user, 200);
    }


    public function update(Request $request, $id)
    {
        $payment = Payment::find($id);
        $payment->name = $request->get('name');
        $payment->amount = $request->get('amount');
        $payment->mandatory = $request->get('mandatory');
        $payment->save();
    }

    public function addStudentPayment(Request $request) {
        $user = User::find($request->get('student'));
        $payment = Payment::find($request->get('payment'));
        $user->studentPayments()->attach(
            $request->get('payment'),
            [
                'date' => $request->get('date'),
                'payment' => $request->get('amount'),
                'quantity' => $request->get('quantity'),
                'discount' => $request->get('discount'),
                'total' => $request->get('total'),
                //'typesetter' => Auth::user()->id,
            ]
        );
    }

    
    public function userPayments()
    {
        $userPayments = DB::table('payment_users as pu')
            ->join('users as u', 'u.id', '=', 'pu.user_id')
            ->join('payments as p', 'p.id', '=', 'pu.payment_id')
            ->select([
                'pu.id', 
                'pu.date',
                'pu.created_at as createdAt',
                'pu.payment as amount',
                'pu.quantity',
                'pu.discount',
                'pu.total',
                'u.id as userId',
                'u.user',
                'u.name as userName',
                'p.id as paymentId',
                'p.name as paymentName',
            ])
            ->where('pu.deleted_at', null)
            ->orderBy('date', 'desc')
            ->get();
        return $userPayments;
    }

    
    public function editUserPayments(Request $request, $id)
    {
        DB::table('payment_users as pu')
            ->where('pu.id', $id)
            ->update([
                'user_id' => $request->get('student'),
                'payment_id' => $request->get('payment'),
                'date' => $request->get('date'),
                'payment' => $request->get('amount'),
                'quantity' => $request->get('quantity'),
                'discount' => $request->get('discount'),
                'total' => $request->get('total'),
            ]);
        return response()->noContent(200);
    }

    public function deleteUserPayments($id)
    {
        DB::table('payment_users as pu')
            ->where('pu.id', $id)
            ->update([
                'deleted_at' => now()
            ]);
        return response()->noContent(200);
    }
}
