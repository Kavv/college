<?php

namespace App\Http\Controllers;

use App\Models\Cycles;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CycleController extends Controller
{
    //

    public function index() {
        $cycles = Cycles::orderBy('id', 'asc')->get();
        return response()->json($cycles, 200);
    }

    public function create(Request $request) {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        return Cycles::create($request->all());
    }

    public function update(Request $request, $id) {
        $cycle = Cycles::find($id);
        $cycle->update($request->all());

        return $cycle;
    }

    public function delete(Request $request, $id) {
        return Cycles::destroy($id);
    }

    public function generateStructure(Request $request) {
        $request->validate([
            'cycle_id' => 'required'
        ]);

        $checkGroup = Group::where('cycle_id', '=', $request->cycle_id)->first();

        if ($checkGroup) {
            return response()->json(['message' => 'Estructura de ciclo ya generada'], 400);
        }



        $letters = ['A', 'B'];
        $grades = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

        foreach ($grades as $grade) {
            foreach ($letters as $letter) {
                $group = new Group();
                $group->cycle_id = $request->cycle_id;
                $group->name = "Grado {$grade} - Seccion {$letter}";
                $group->description = "No description";
                $group->save();
            }
        }

        $cycle = Cycles::where('id', '=', $request->cycle_id)->first();

        $cycleGroups = Group::where('cycle_id', '=', $request->cycle_id)->get();

        return response()->json(['cycle'=>$cycle, 'groups'=>$cycleGroups], 200);
    }

    public function getStructure(Request $request) {
        $request->validate(
            ['cycle_id' => 'required']
        );

        return Group::where('cycle_id', '=', $request->cycle_id)->orderBy('id', 'asc')->get();
    }
}
