<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Area;

class AreaController extends Controller
{
    //
    public function index() {
        return Area::orderBy('id', 'asc')->get();
    }

    public function create(Request $request) {
        $request->validate([
            'name' => 'required'
        ]);

        return Area::create($request->all());
    }

    public function update(Request $request, $id) {
        $area = Area::find($id);

        $area->update($request->all());

        return $area;
    }

    public function delete(Request $request, $id) {
        return Area::destroy($id);
    }


}
