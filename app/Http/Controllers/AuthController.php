<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    // JWT Authentication Controller to College API

    /**
     * Create new AuthController instance
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except'=>['login', 'register']]);
    }

    /**
     * Register a user
     * @return \Illuminate\Http\JsonResponse
     */
    public function register() {
        $validator = Validator::make(request()->all(), [
            'name' => 'required',
            'user' => 'required|unique:users',
            'password' => 'required|min:12',
            'type_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = new User;
        $user->name = request()->name;
        $user->user = request()->user;
        $user->password = bcrypt(request()->password);
        $user->type_id = request()->type_id;
        $user->save();

        return response()->json($user, 201);
    }

    /**
     * Get a JWT via given credentials
     * @return JsonResponse
     */
    public function login() {
        $credentials = request(['user', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = auth()->user();
        
        return $this->respondWithToken([
            'user' => $user,
            'token' => $token
        ]);
    }

    /**
     * Get Authenticated user
     * @return JsonResponse
     */
    public function me() {
        return response()->json(auth()->user());
    }

    /**
     * Log out user (Invalidate JWT Token)
     * @return JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message'=>'Successfully Logged Out']);
    }

    /**
     * Refresh Token
     * @return JsonResponse
     */
    public function refresh() {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get token
     * @param string $token
     * @return JsonResponse
     */
    public function respondWithToken($token) {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires' => auth('api')->factory()->getTTL() * 60
        ]);
    }

    public function changePassword(Request $request) {
        $user = auth()->user();
        $currentPassword = $request->get('currentPassword');
        $newPassword = $request->get('newPassword');

        $credentials = [
            'user' => $user->user,
            'password' => $currentPassword,
        ];
        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Actual contraseña no es correcta'], 422);
        }

        $recordUser = User::find($user->id);
        $recordUser->password = bcrypt($newPassword);
        $recordUser->save();
        return response()->json(['message' => 'Contraseña actualizada'], 200);
    }
}
