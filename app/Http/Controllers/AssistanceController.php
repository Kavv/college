<?php

namespace App\Http\Controllers;

use App\Models\Assistance;
use Illuminate\Http\Request;

class AssistanceController extends Controller
{
    // Assistance Controller

    public function index() {
        return Assistance::orderBy('id', 'asc')->get();
    }

    public function create(Request $request) {
        $request->validate([
            'date' => 'required',
            'status' => 'required',
            'teacher_id' => 'required',
            'student_id' => 'required',
            'group_id' => 'required',
            'cycle_id' => 'required'
        ]);

        return Assistance::create($request->all());
    }

    public function update(Request $request, $id) {
        $assistance = Assistance::find($id);

        $assistance->update($request->all());

        return $assistance;
    }

    public function delete(Request $request, $id) {
        return Assistance::destroy($id);
    }
}
