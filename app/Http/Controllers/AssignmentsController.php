<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AssignmentsController extends Controller
{
    public function index(Request $request)
    {
        /* $user = Auth::user();
        $teacher = DB::table('teacher_user as su')
            ->select('su.group_id')
            ->where('user_id', $user->id)
            ->first(); */
        $teacher = null;
        $query = Assignment::join('assignments_groups as ag', 'ag.assignment_id', '=', 'assignments.id')
        ->join('groups', 'ag.group_id', '=', 'groups.id')
        ->join('cycles', 'cycles.id', '=', 'groups.cycle_id')
        ->leftjoin('areas', 'areas.id', '=', 'assignments.area_id')
        ->select([
            'assignments.id',
            'assignments.name',
            'assignments.description',
            'assignments.limit_time as limitTime',
            'assignments.start_date as startDate',
            'assignments.url',
            'assignments.points',
            'assignments.area_id as area',
            'areas.name as areaName',
            'groups.id as group',
            'groups.name as groupName',
            'cycles.id as cycle',
            'cycles.name as cycleName',
            'assignments.created_at as createdAt',
        ]);
        if ($request->get('cycle')) {
            $query = $query->where('cycles.id', '=', $request->get('cycle'));
        }
        if ($request->get('area')) {
            $query = $query->where('assignments.area_id', '=', $request->get('area'));
        }
        if ($request->get('group') || $teacher) {
            // This commented line filter the assignment only by its respective group
            //$group = $teacher ? $teacher->group_id : $request->get('group');
            $group = $request->get('group');
            $query = $query->where('groups.id', '=', $group);
        }
        $assignments = $query->get();
        return response()->json($assignments, 200);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->get('group') as $group) {
                $assignments = Assignment::create([
                    'name' => $request->get('name'),
                    'description' => $request->get('description'),
                    'points' => $request->get('points'),
                    'url' => $request->get('url'),
                    'start_date' => $request->get('startDate'),
                    'limit_time' => $request->get('limitTime'),
                    'area_id' => $request->get('area'),
                ]);
                $assignments->groups()->attach($group);
            }
            DB::commit();
            return response()->json($assignments, 200);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => $th
            ], 500);
        }
    }

    public function show($id)
    {
        return Assignment::find($id);
    }

    public function update(Request $request, $id)
    {
        $assignments = Assignment::find($id);
        $assignments->update([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'points' => $request->get('points'),
            'url' => $request->get('url'),
            'start_date' => $request->get('startDate'),
            'limit_time' => $request->get('limitTime'),
            'area_id' => $request->get('area'),
        ]);
        $assignments->save();
    }

    public function destroy($id)
    {
        $assignments = Assignment::find($id);
        $assignments->delete();
        return response()->noContent(200);
    }
}
