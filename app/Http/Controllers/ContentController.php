<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Area;
use App\Models\Group;
use App\Models\Cycles;
use App\Models\ContentGroup;

class ContentController extends Controller
{
    //
    public function index() {
        return Content::orderBy('id', 'asc')->get();
    }

    public function create(Request $request) {
        $request->validate([
            'cycle_id' => 'required',
            'group_id' => 'required',
            'area_id' => 'required',
            'title' => 'required',
        ]);

        $cycle = Cycles::find($request->cycle_id);
        $group = Group::find($request->group_id);
        $area = Area::find($request->area_id);
        $content = new Content;
        $content->title = $request->title;
        $content->subtitle = $request->subtitle;
        $content->description = $request->description;
        $content->area_id = $request->area_id;
        $content->save();

        $content_group = new ContentGroup;
        $content_group->group_id = $group->id;
        $content_group->content_id = $content->id;
        $content_group->save();

        return $content;
    }

    public function update(Request $request, $id) {
        $content = Content::find($id);

        if ($content->group_id != $request->group_id) {
            $content_group = ContentGroup::where('group_id', '=', $content->group_id)
                                        ->where('content_id', '=', $content->id)->first();
            $content_group->group_id = $request->group_id;
            $content_group->save();
        }
        $content->update($request->all());

        return $content;
    }

    public function delete(Request $request, $id) {
        return Content::destroy($id);
    }
}
