<?php

namespace App\Http\Controllers;

use App\Models\Cycles;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    //lista de estudiantes por ciclo
    public function getStudentsByDateRange(Request $request) {
        $start = $request->get('start');
        $end = $request->get('end');

        $students = DB::select('Select
            c.id "Ciclo ID",
            TO_CHAR(u.created_at, \'DD/MM/YYYY\') "Digitado",
            c.name "Ciclo",
            g.name "Grupo",
            u.user "Usuario",
            u.name "Nombre",
            su.father "Padre",
            su.mother "Madre",
            su.birthday "Nacimiento",
            su.gender "Genero"
            from users as u
                left join student_user su on u.id = su.user_id
                left join group_users gu on u.id = gu.user_id
                left join groups g on gu.group_id = g.id
                left join cycles c on g.cycle_id = c.id
                    where u.type_id = 1 AND u.deleted_at is null AND u.created_at BETWEEN ? AND ?
                    order by c.id asc, g.name asc, u.name asc;', [$start, $end]);

        $excludedKeys = ['Ciclo ID'];
        if (count($students) === 0) {
            return response()->json(['message' => 'No hay datos a exportar'], 422);
        }
        $keys = get_object_vars($students[0]);
        foreach ($excludedKeys as $value) {
            unset($keys[$value]);
        }
        $keys = array_keys($keys);
        $startRange = date('d/m/Y', strtotime($start));
        $endRange = date('d/m/Y', strtotime($end));
        $title = "ESTUDIANTES DIGITADOS ENTRE $startRange - $endRange";
        $subtitle = 'Reporte de todos los alumnos registrados en el colegio Hipano-Americano';
        $cellWidth = ['10%','10%','10%', '10%','15%','15%','15%','10%','5%'];
        $data = [
            'students' => $students,
            'keys' => $keys,
            'title' => $title,
            'subtitle' => $subtitle,
            'cellWidth' => $cellWidth
        ];
        $dompdf = App::make("dompdf.wrapper");
        $dompdf->setPaper('A4', 'landscape');
        $pdf = $dompdf->loadView('reports/allStudents', compact('data'));
        return $pdf->download('documento.pdf');
    }
    //lista de estudiantes por ciclo
    public function getStudentsByCycle(Request $request) {
        $id = $request->get('cycle');

        $cycle = Cycles::find($id);

        $students = DB::select('Select
            c.id "Ciclo ID",
            TO_CHAR(u.created_at, \'DD/MM/YYYY\') "Digitado",
            c.name "Ciclo",
            g.name "Grupo",
            u.user "Usuario",
            u.name "Nombre",
            su.father "Padre",
            su.mother "Madre",
            su.birthday "Nacimiento",
            su.gender "Genero"
            from users as u
                left join student_user su on u.id = su.user_id
                left join group_users gu on u.id = gu.user_id
                left join groups g on gu.group_id = g.id
                left join cycles c on g.cycle_id = c.id
                    where u.type_id = 1 AND u.deleted_at is null AND c.id = ?
                    order by c.id asc, g.name asc, u.name asc;', [$id]);

        $excludedKeys = ['Ciclo ID'];
        if (count($students) === 0) {
            return response()->json(['message' => 'No hay datos a exportar'], 422);
        }
        $keys = get_object_vars($students[0]);
        foreach ($excludedKeys as $value) {
            unset($keys[$value]);
        }
        $keys = array_keys($keys);
        $title = "ESTUDIANTES DEL CICLO " . strtoupper($cycle->name);
        $subtitle = 'Reporte de todos los alumnos que forman parte del ciclo ' . strtoupper($cycle->name);
        $cellWidth = ['10%','10%','10%', '10%','15%','15%','15%','10%','5%'];
        $data = [
            'students' => $students,
            'keys' => $keys,
            'title' => $title,
            'subtitle' => $subtitle,
            'cellWidth' => $cellWidth
        ];
        $dompdf = App::make("dompdf.wrapper");
        $dompdf->setPaper('A4', 'landscape');
        $pdf = $dompdf->loadView('reports/allStudents', compact('data'));
        return $pdf->download('documento.pdf');
    }
    //Lista de estudiantes por grupo
    public function getStudentsByGroup(Request $request) {
        $id = $request->get('group');

        $cycle = Group::find($id);

        $students = DB::select('Select
            c.id "Ciclo ID",
            TO_CHAR(u.created_at, \'DD/MM/YYYY\') "Digitado",
            c.name "Ciclo",
            g.name "Grupo",
            u.user "Usuario",
            u.name "Nombre",
            su.father "Padre",
            su.mother "Madre",
            su.birthday "Nacimiento",
            su.gender "Genero"
            from users as u
                left join student_user su on u.id = su.user_id
                left join group_users gu on u.id = gu.user_id
                left join groups g on gu.group_id = g.id
                left join cycles c on g.cycle_id = c.id
                    where u.type_id = 1 AND u.deleted_at is null AND g.id = ?
                    order by c.id asc, g.name asc, u.name asc;', [$id]);

        $excludedKeys = ['Ciclo ID'];
        if (count($students) === 0) {
            return response()->json(['message' => 'No hay datos a exportar'], 422);
        }
        $keys = get_object_vars($students[0]);
        foreach ($excludedKeys as $value) {
            unset($keys[$value]);
        }
        $keys = array_keys($keys);
        $title = "ESTUDIANTES DEL CICLO " . strtoupper($cycle->name);
        $subtitle = 'Reporte de todos los alumnos que forman parte del ciclo ' . strtoupper($cycle->name);
        $cellWidth = ['10%','10%','10%', '10%','15%','15%','15%','10%','5%'];
        $data = [
            'students' => $students,
            'keys' => $keys,
            'title' => $title,
            'subtitle' => $subtitle,
            'cellWidth' => $cellWidth
        ];
        $dompdf = App::make("dompdf.wrapper");
        $dompdf->setPaper('A4', 'landscape');
        $pdf = $dompdf->loadView('reports/allStudents', compact('data'));
        return $pdf->download('documento.pdf');
    }

    public function getAssistanceByCycle(Request $request) {
        $groupId = $request->get('group');

        $group = Group::find($groupId);

        $assistances = DB::select('Select
            c.id "Ciclo ID",
            c.name "Ciclo",
            g.id "GrupoID",
            g.name "Grupo",
            u.name "Estudiante",
            u.user "Usuario",
            TO_CHAR(a.date, \'DD/MM/YYYY\') "Fecha",
            a.status "Estado",
            teachers.name "Profesor",
            TO_CHAR(a.created_at, \'DD/MM/YYYY\') "Digitado"
            from users as u
                inner join student_user su on u.id = su.user_id
                inner join group_users gu on u.id = gu.user_id and gu.deleted_at is null
                inner join groups g on gu.group_id = g.id
                inner join cycles c on g.cycle_id = c.id
                inner join assistance a on a.student_id = u.id
                inner join users teachers on a.teacher_id = teachers.id
                    where u.type_id = 1 AND u.deleted_at is null
                    order by g.name, u.name asc, a.date asc');

        if (count($assistances) === 0) {
            return response()->json(['message' => 'No hay datos a exportar'], 422);
        }
        $first = $assistances[0];
        $title = "ASISTENCIAS DEL CICLO " . strtoupper($assistances[0]->Ciclo);
        $subtitle = '';

        $excludedKeys = ['Ciclo ID', "Ciclo"];
        $keys = get_object_vars($assistances[0]);
        foreach ($excludedKeys as $value) {
            unset($keys[$value]);
        }
        $keys = array_keys($keys);
        $cellWidth = ['25%','18%','12%','13%','20%', '12%'];


        $groupAssistances = [];
        foreach ($assistances as $key => $data) {
            $key = 'GRUPO-'.$data->GrupoID;
            $groupAssistances[$key]['estudiantes'][$data->Usuario][] = $data;
            $groupAssistances[$key]['nombre'] = $data->Grupo;
        }
        $data = [
            'assistances' => $groupAssistances,
            'keys' => $keys,
            'title' => $title,
            'subtitle' => $subtitle,
            'cellWidth' => $cellWidth
        ];
        $dompdf = App::make("dompdf.wrapper");
        //$dompdf->setPaper('A4', 'landscape');
        $pdf = $dompdf->loadView('reports/assistancesByCycle', compact('data'));
        return $pdf->download('documento.pdf');
    }

    public function getAssistanceByGroup(Request $request) {
        $groupId = $request->get('group');

        $group = Group::find($groupId);

        $assistances = DB::select('Select
            c.id "Ciclo ID",
            c.name "Ciclo",
            u.name "Estudiante",
            u.user "Usuario",
            TO_CHAR(a.date, \'DD/MM/YYYY\') "Fecha",
            a.status "Estado",
            teachers.name "Profesor",
            TO_CHAR(a.created_at, \'DD/MM/YYYY\') "Digitado"
            from users as u
                inner join student_user su on u.id = su.user_id
                inner join group_users gu on u.id = gu.user_id and gu.deleted_at is null
                inner join groups g on gu.group_id = g.id
                inner join cycles c on g.cycle_id = c.id
                inner join assistance a on a.student_id = u.id
                inner join users teachers on a.teacher_id = teachers.id
                    where u.type_id = 1 AND u.deleted_at is null AND g.id = ?
                    order by u.name asc, a.date asc', [$groupId]);

        if (count($assistances) === 0) {
            return response()->json(['message' => 'No hay datos a exportar'], 422);
        }
        $first = $assistances[0];
        $title = "ASISTENCIAS DEL GRUPO " . strtoupper($group->name);
        $subtitle = 'CICLO ' . strtoupper($assistances[0]->Ciclo);

        $excludedKeys = ['Ciclo ID', "Ciclo"];
        $keys = get_object_vars($assistances[0]);
        foreach ($excludedKeys as $value) {
            unset($keys[$value]);
        }
        $keys = array_keys($keys);
        $cellWidth = ['25%','18%','12%','13%','20%', '12%'];


        $groupAssistances = [];
        foreach ($assistances as $key => $data) {
            $groupAssistances[$data->Usuario][] = $data;
        }

        $data = [
            'assistances' => $groupAssistances,
            'keys' => $keys,
            'title' => $title,
            'subtitle' => $subtitle,
            'cellWidth' => $cellWidth
        ];
        $dompdf = App::make("dompdf.wrapper");
        //$dompdf->setPaper('A4', 'landscape');
        $pdf = $dompdf->loadView('reports/assistancesByGroup', compact('data'));
        return $pdf->download('documento.pdf');
    }

    public function getAssistanceByStudent(Request $request) {
        $groupId = $request->get('group')['id'];
        $studentId = $request->get('student');

        $group = Group::find($groupId);

        $assistances = DB::select('Select
            c.id "Ciclo ID",
            c.name "Ciclo",
            u.name "Estudiante",
            u.user "Usuario",
            TO_CHAR(a.date, \'DD/MM/YYYY\') "Fecha",
            a.status "Estado",
            teachers.name "Profesor",
            TO_CHAR(a.created_at, \'DD/MM/YYYY\') "Digitado"
            from users as u
                left join student_user su on u.id = su.user_id
                left join group_users gu on u.id = gu.user_id
                left join groups g on gu.group_id = g.id
                left join cycles c on g.cycle_id = c.id
                left join assistance a on a.student_id = u.id
                left join users teachers on a.teacher_id = teachers.id
                    where u.type_id = 1 AND u.deleted_at is null AND g.id = ? AND su.id = ?
                    order by c.id asc, g.name asc, u.name asc;', [$groupId, $studentId]);

        if (count($assistances) === 0) {
            return response()->json(['message' => 'No hay datos a exportar'], 422);
        }
        $first = $assistances[0];
        $title = "ASISTENCIAS DEL ESTUDIANTE " . strtoupper($first->Estudiante) . ' / ' . $first->Usuario;
        $subtitle = 'CICLO ' . strtoupper($assistances[0]->Ciclo) . ' - GRUPO ' . strtoupper($group->name);

        $excludedKeys = ['Ciclo ID', "Ciclo", "Estudiante", "Usuario"];
        $keys = get_object_vars($assistances[0]);
        foreach ($excludedKeys as $value) {
            unset($keys[$value]);
        }
        $keys = array_keys($keys);
        $cellWidth = ['25%','20%','25%', '25%'];
        $data = [
            'assistances' => $assistances,
            'keys' => $keys,
            'title' => $title,
            'subtitle' => $subtitle,
            'cellWidth' => $cellWidth
        ];
        $dompdf = App::make("dompdf.wrapper");
        //$dompdf->setPaper('A4', 'landscape');
        $pdf = $dompdf->loadView('reports/assistancesByStudent', compact('data'));
        return $pdf->download('documento.pdf');
    }

    // Notas de un estudiante
    public function resultByStudent() {

    }
    // Notas de todos los estudiantes de un grupo
    public function resultByGroup() {

    }

    // Notas de todos los estudiantes en todos los grupos
    public function resultByGroups() {

    }

    // Nota de estudiante por ciclo
    public function resultByCycle() {

    }


    public function pagosPendientesPorGrupo() {

    }

    public function pagosPendientesPorCiclo() {

    }

    public function todosLosPagosPendientes() {

    }

    public function pagoPendientePorEstudiante() {

    }

    public function listaDocentesPorCiclo() {

    }

    public function listaDocentesPorGrupo() {

    }

    public function listadoDeTareasPorGrupo() {

    }

    public function listadoDeTareasPorCiclo() {

    }
    
    public function pagosEfectuados() {

    }

    public function pagosDeMensualidades() {

    }

    public function pagosPendientesMensualidades() {

    }

    public function tareasSinRevisar() {

    }

    public function resultadoPorTareas() {

    }

    public function estructuraDeGruposPorCiclo() {

    }

    // En los datos del estudiante definir el turno: si es matutino o vespertino
}
