<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovementType extends Model
{
    use SoftDeletes;

    protected $table = 'movement_type';
    public $primaryKey = 'id';
    public $incrementing = true;

    protected $fillable = [
        'name'
    ];
}
