<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class StaffUser extends Model
{

    protected $table = 'staff_user';
    public $primaryKey = 'id';
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gender',
        'birthday',
        'dni',
        'phone',
        'position',
        'institution',
        'salary',
        'address',
    ];


}
