<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Score extends Model
{
    use SoftDeletes;
    protected $table = 'scores';
    public $primaryKey = 'id';
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'assignment_id',
        'user_id',
        'points',
        'comments',
        'explanation',
        'answer',
        'file',
    ];

    public $appends = [
        'documents',
    ];

    public function getDocumentsAttribute() {
        return DocumentScore::where('score_id', $this->id)->get();
    }
}
