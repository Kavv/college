<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class StudentUser extends Model
{

    protected $table = 'student_user';
    public $primaryKey = 'id';
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'father',
        'mother',
        'father_phone',
        'mother_phone',
        'birthday',
        'note',
        'gender',
        'phone',
        'user_id',
    ];


}
