<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentGroup extends Model
{
    protected $table = 'content_groups';

    public $primaryKey = 'id';

    public $incrementing = true;

    protected $fillable = [
        'group_id',
        'content_id'
    ];
}
