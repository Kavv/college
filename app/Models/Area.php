<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'areas';
    public $primaryKey = 'id';
    public $incrementing = true;

    protected $fillable = [
        'name',
    ];
}
