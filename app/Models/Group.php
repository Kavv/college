<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Group extends Model
{
    protected $table = 'groups';

    public $primaryKey = 'id';

    public $incrementing = true;

    public $appends = ['cycle', 'teacher', 'students'];

    protected $fillable = [
        'cycle_id',
        'name',
        'description'
    ];

    public function getCycleAttribute() {
        return Cycles::where('id', '=', $this->cycle_id)->first();
    }

    public function getTeacherAttribute() {
        return DB::table('group_users')
        ->join('users', 'group_users.user_id', '=', 'users.id')
        ->join('teacher_user', 'teacher_user.user_id', '=', 'users.id')
        ->where('group_users.group_id', '=', $this->id)
        ->where('users.type_id', '=', 2)
        ->select('users.name', 'teacher_user.*')
        ->get();
    }

    public function getStudentsAttribute() {
        return DB::table('group_users')
        ->join('users', 'group_users.user_id', '=', 'users.id')
        ->join('student_user', 'student_user.user_id', '=', 'users.id')
        ->where('group_users.group_id', '=', $this->id)
        ->where('users.type_id', '=', 1)
        ->where('group_users.deleted_at', '=', null)
        ->select('users.name', 'users.user as code', 'student_user.*')
        ->get();
    }

    public function teachers()
    {
        return $this->hasMany(TeacherUser::class);
    }
}
