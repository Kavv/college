<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use SoftDeletes;
    protected $table = 'assignments';
    public $primaryKey = 'id';
    public $incrementing = true;
    
    protected $fillable = [
        'name',
        'description',
        'points',
        'url',
        'start_date',
        'limit_time',
        'area_id',
    ];

    public $appends = [
        'documents',
    ];

    public function groups()
    {
        return $this->belongsToMany(Payment::class, 'assignments_groups', 'assignment_id', 'group_id')->withTimestamps();
    }
    
    public function getDocumentsAttribute() {
        return AssignmentDocument::where('assignment_id', $this->id)->get();
    }
}
