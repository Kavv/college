<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accounts extends Model
{
    use SoftDeletes;

    protected $table = 'accounts';

    public $primaryKey = 'id';
    public $incrementing = true;

    public $appends = ['account_type'];

    protected $fillable = [
        'code',
        'name',
        'type_id'
    ];

    public function getAccountTypeAttribute() {
        $account_type = AccountType::find($this->type_id);
        return $account_type->name;
    }
}
