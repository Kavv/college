<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Documents;

class AssignmentDocument extends Model
{
    protected $table = 'assignments_documents';

    public $appends = ['document'];

    protected $fillable = [
        'assignment_id',
        'document_id'
    ];

    public function getDocumentAttribute() {
        return Documents::where('id', $this->document_id)->first();
    }
}
