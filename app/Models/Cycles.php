<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cycles extends Model
{
    protected $table = 'cycles';

    public $primaryKey = 'id';

    public $incrementing = true;

    protected $fillable = [
        'name',
        'description'
    ];

    protected $appends = array('structure');

    public function getStructureAttribute() {
        $structure = Group::where('cycle_id', '=', $this->id)->get();

        return count($structure) > 0;
    }


}
