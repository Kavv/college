<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Documents;

class DocumentScore extends Model
{
    protected $table = 'documents_scores';

    public $appends = ['document'];

    protected $fillable = [
        'score_id',
        'document_id'
    ];

    public function getDocumentAttribute() {
        return Documents::where('id', $this->document_id)->first();
    }
}
