<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class TeacherUser extends Model
{

    protected $table = 'teacher_user';
    public $primaryKey = 'id';
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gender',
        'birthday',
        'dni',
        'phone',
        'title',
        'institution',
        'salary',
        'address'
    ];

    public $appends = ['user'];

    public function getUserAttribute() {
        return User::where('id', '=', $this->user_id)->first();
    }


}
