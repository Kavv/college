<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    public $appends = ['profile'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user',
        'password',
        'type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAuthIdentifier()
    {
        return $this->user;
    }

    /**
     * Get the student data associated with the user.
     */
    public function studentData()
    {
        return $this->hasOne(StudentUser::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    public function getUserType() {
        $userType = UserType::find($this->user->type_id);

        return $userType->type;
    }
    /**
     * Get the teacher data associated with the user.
     */
    public function teacherData()
    {
        return $this->hasOne(TeacherUser::class);
    }

    /**
     * Get the stagg data associated with the user.
     */
    public function staffData()
    {
        return $this->hasOne(StaffUser::class);
    }


    public function studentPayments()
    {
        return $this->belongsToMany(Payment::class, 'payment_users', 'user_id', 'payment_id')->withPivot('date' ,'payment' ,'quantity' ,'discount' ,'total', 'typesetter')->withTimestamps();
    }

    public function teacherAreas()
    {
        return $this->belongsToMany(Area::class, 'area_users', 'user_id', 'area_id');
    }

    public function teacherGroup()
    {
        return $this->belongsTo(Group::class);
    }

    public function getProfileAttribute() {
        return $this->user;
    }

    public function studentGroups()
    {
        return $this->belongsToMany(Group::class, 'group_users', 'user_id', 'group_id')->withTimestamps();
    }
}
