<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assistance extends Model
{
    protected $table = 'assistance';

    public $primaryKey = 'id';
    
    public $incrementing = true;

    public $appends = [
        'student',
        'teacher',
        'group',
        'cycle'
    ];

    protected $fillable = [
        'date',
        'status',
        'teacher_id',
        'student_id',
        'group_id',
        'cycle_id'
    ];

    public function getStudentAttribute() {
        return User::where('id', $this->student_id)->first();
    }

    public function getTeacherAttribute() {
        return User::where('id', $this->teacher_id)->first();
    }

    public function getGroupAttribute() {
        return Group::where('id', $this->group_id)->first();
    }

    public function getCycleAttribute() {
        return Cycles::where('id', $this->cycle_id)->first();
    }
}
