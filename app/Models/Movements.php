<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movements extends Model
{
    use SoftDeletes;

    protected $table = 'movements';

    public $primaryKey = 'id';
    public $incrementing = true;

    public $appends = [
        'debit_account',
        'credit_account',
        'creator'
    ];

    protected $fillable = [
        'date',
        'description',
        'currency',
        'amount',
        'notes',
        'type',
        'created_by',
        'origin_account',
        'destination_account'
    ];

    public function getDebitAccountAttribute() {
        return Accounts::find($this->origin_account);
    }

    public function getCreditAccountAttribute() {
        return Accounts::find($this->destination_account);
    }

    public function getCreatorAttribute() {
        return User::find($this->created_by);
    }
}
