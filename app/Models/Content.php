<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ContentDocument;

class Content extends Model
{
    protected $table = 'contents';

    public $primaryKey = 'id';

    public $incrementing = true;

    public $appends = [
        'documents',
        'area',
        'cycle_id',
        'group_id'
    ];

    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'area_id'
    ];

    public function getDocumentsAttribute() {
        return ContentDocument::where('content_id', $this->id)->get();
    }

    public function getAreaAttribute() {
        return Area::where('id', $this->area_id)->first();
    }

    public function getCycleIdAttribute() {
        $content_group = ContentGroup::where('content_id', $this->id)->first();
        $group = Group::where('id', $content_group->group_id)->first();
        return $group->cycle_id;
    }

    public function getGroupIdAttribute() {
        $content_group = ContentGroup::where('content_id', $this->id)->first();
        return $content_group->group_id;
    }

}
