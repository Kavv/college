<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserType extends Model
{
    use HasFactory;

    protected $table = 'user_type';

    public $primaryKey = 'id';

    public $incrementing = true;

    protected $fillable = [
        'type'
    ];


}
